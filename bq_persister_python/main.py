import pandas as pd
import pandas_gbq
import base64
import json
import re


def forecast_upload_bq(event, context):
    # Get all data
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    data_extract = json.loads(pubsub_message)

    # print(type(data_extract))
    print(data_extract['table'])

    name_table_extract = data_extract['table']

    # Test table
    if (re.search('fc_', name_table_extract, re.IGNORECASE) and name_table_extract != "fc_raw"):
        print(name_table_extract)
        projectid = 'pluvion-tech'
        dt = data_extract['data']
        print(type(dt))
        print(dt)
        df = pd.DataFrame(dt)
        name_table = 'pluvion_lab.' + name_table_extract + '_temporary'
        print('created df')
        pandas_gbq.to_gbq(
            df, name_table, project_id=projectid, if_exists='append')
        print('\o/')
