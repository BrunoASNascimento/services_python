import base64
import json
from google.cloud import firestore
import google.cloud.exceptions
from datetime import datetime
import re


def forecast_upload_fs(event, context):
    # Get data
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    data_extract = json.loads(pubsub_message)

    print(type(data_extract))
    print(data_extract['table'])

    name_collection = data_extract['table']

    # Test collection
    if (re.search('fc_', name_collection, re.IGNORECASE) and name_collection != "fc_raw"):
        db = firestore.Client()
        print(name_collection)
        for dt in data_extract['data']:
            fcst_date = datetime.strptime(
                dt['forecastedAtISO'], '%Y-%m-%dT%H:%M:%S.%fZ')
            name_document = fcst_date.strftime(
                '%Y%m%d_')+str(dt['itemId'])+str(dt['latitude'])+'_'+str(dt['longitude'])
            db.collection(name_collection).document(name_document).set(dt)
        print('\o/')
